import pigpio
import time

mypi = pigpio.pi()

class relay_controller():

	def __init__(self):
		self.mypi = pigpio.pi()
		self.relays = [21, 20, 26, 16, 19, 13, 12, 6]
		self.relay_states = [-1,-1,-1,-1,-1,-1,-1,-1]

	def reset(self):
		for i in range(0, len(self.relays)):
			self.mypi.write(self.relays[i], 1)

	def smoke_test(self):
		self.reset()
		for i in range(0, len(self.relays)):
			self.mypi.write(self.relays[i], 0)
			time.sleep(0.25)

		for i in range(0, len(self.relays)):
			self.mypi.write(self.relays[i], 1)
			time.sleep(0.25)
		self.reset()
		
	def relay_off(self, no):
		no -= 1
		self.mypi.write(self.relays[no], 0)
		
	def relay_on(self, no):
		no -= 1
		self.mypi.write(self.relays[no], 1)
	
	def relay_off_on(self, no):
		no -= 1
		self.mypi.write(self.relays[no], 0)
		time.sleep(10)
		self.mypi.write(self.relays[no], 1)

	def get_relay_states(self):
		for i in range(0, len(self.relays)):
			self.relay_states[i] = self.mypi.read(self.relays[i])
		return self.relay_states
    

if __name__ == '__main__':
	
	myswitch = relay_controller()
	myswitch.reset()
	myswitch.smoke_test()
	print(myswitch.get_relay_states())