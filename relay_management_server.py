from twisted.web.server import Site
from twisted.web.resource import Resource
from twisted.internet import reactor, endpoints
import relay_management
import web_helper

import cgi
placeholders = ["placeholder1", "placeholder2", "placeholder3", "placeholder4", "placeholder5", "placeholder6", "placeholder7", "placeholder8"]
captions = ["Schalter 1", "Schalter 2", "Schalter 3", "Schalter 4", "Schalter 5", "Schalter 6", "Schalter 7", "Schalter 8"]

class RelayPage(Resource):

	def render_GET(self, request):
		content = """
				<h1>Menu</h1>
				<div class="row" style="padding-bottom: 10px">
				<div class="col-md-4"><button class="btn btn-default btn-block btn-danger" onclick="post('/relay', {'code': '01_off'})">placeholder1 aus</button></div>
				<div class="col-md-4"><button class="btn btn-default btn-block btn-success" onclick="post('/relay', {'code': '01_on'})">placeholder1 an</button></div>
				<div class="col-md-4"><button class="btn btn-default btn-block btn-primary" onclick="post('/relay', {'code': '01_off_on'})">placeholder1 auto aus an</button></div>
				</div>
				<div class="row" style="padding-bottom: 10px">
				<div class="col-md-4"><button class="btn btn-default btn-block btn-danger" onclick="post('/relay', {'code': '02_off'})">placeholder2 aus</button></div>
				<div class="col-md-4"><button class="btn btn-default btn-block btn-success" onclick="post('/relay', {'code': '02_on'})">placeholder2 an</button></div>
				<div class="col-md-4"><button class="btn btn-default btn-block btn-primary" onclick="post('/relay', {'code': '02_off_on'})">placeholder2 auto aus an</button></div>
				</div>
				<div class="row" style="padding-bottom: 10px">
				<div class="col-md-4"><button class="btn btn-default btn-block btn-danger" onclick="post('/relay', {'code': '03_off'})">placeholder3 aus</button></div>
				<div class="col-md-4"><button class="btn btn-default btn-block btn-success" onclick="post('/relay', {'code': '03_on'})">placeholder3 an</button></div>
				<div class="col-md-4"><button class="btn btn-default btn-block btn-primary" onclick="post('/relay', {'code': '03_off_on'})">placeholder3 auto aus an</button></div>
				</div>
				<div class="row" style="padding-bottom: 10px">
				<div class="col-md-4"><button class="btn btn-default btn-block btn-danger" onclick="post('/relay', {'code': '04_off'})">placeholder4 aus</button></div>
				<div class="col-md-4"><button class="btn btn-default btn-block btn-success" onclick="post('/relay', {'code': '04_on'})">placeholder4 an</button></div>
				<div class="col-md-4"><button class="btn btn-default btn-block btn-primary" onclick="post('/relay', {'code': '04_off_on'})">placeholder4 auto aus an</button></div>
				</div>
				<div class="row" style="padding-bottom: 10px">
				<div class="col-md-4"><button class="btn btn-default btn-block btn-danger" onclick="post('/relay', {'code': '05_off'})">placeholder5 aus</button></div>
				<div class="col-md-4"><button class="btn btn-default btn-block btn-success" onclick="post('/relay', {'code': '05_on'})">placeholder5 an</button></div>
				<div class="col-md-4"><button class="btn btn-default btn-block btn-primary" onclick="post('/relay', {'code': '05_off_on'})">placeholder5 auto aus an</button></div>
				</div>
				<div class="row" style="padding-bottom: 10px">
				<div class="col-md-4"><button class="btn btn-default btn-block btn-danger" onclick="post('/relay', {'code': '06_off'})">placeholder6 aus</button></div>
				<div class="col-md-4"><button class="btn btn-default btn-block btn-success" onclick="post('/relay', {'code': '06_on'})">placeholder6 an</button></div>
				<div class="col-md-4"><button class="btn btn-default btn-block btn-primary" onclick="post('/relay', {'code': '06_off_on'})">placeholder6 auto aus an</button></div>
				</div>
				<div class="row" style="padding-bottom: 10px">
				<div class="col-md-4"><button class="btn btn-default btn-block btn-danger" onclick="post('/relay', {'code': '07_off'})">placeholder7 aus</button></div>
				<div class="col-md-4"><button class="btn btn-default btn-block btn-success" onclick="post('/relay', {'code': '07_on'})">placeholder7 an</button></div>
				<div class="col-md-4"><button class="btn btn-default btn-block btn-primary" onclick="post('/relay', {'code': '07_off_on'})">placeholder7 auto aus an</button></div>
				</div>
				<div class="row" style="padding-bottom: 10px">
				<div class="col-md-4"><button class="btn btn-default btn-block btn-danger" onclick="post('/relay', {'code': '08_off'})">placeholder8 aus</button></div>
				<div class="col-md-4"><button class="btn btn-default btn-block btn-success" onclick="post('/relay', {'code': '08_on'})">placeholder8 an</button></div>
				<div class="col-md-4"><button class="btn btn-default btn-block btn-primary" onclick="post('/relay', {'code': '08_off_on'})">placeholder8 auto aus an</button></div>
				</div>
				<button class="btn btn-default" onclick="post('/relay', {'code': 'smoke_test'})">smoke test</button>
	"""
		for i in range(0, len(placeholders)):
			content = content.replace(placeholders[i], captions[i])
		return web_helper.get_page(content)

	def render_POST(self, request):
		code = str(request.args["code".encode('utf-8')][0], "utf-8")
		#print(code)
		myrelay = relay_management.relay_controller()
		if(code == 'smoke_test'):
			myrelay.smoke_test()
			
		if(code == '01_off'):
			myrelay.relay_off(1)
		if(code == '01_on'):
			myrelay.relay_on(1)
		if(code == '01_off_on'):
			myrelay.relay_off_on(1)
			
		if(code == '02_off'):
			myrelay.relay_off(2)
		if(code == '02_on'):
			myrelay.relay_on(2)
		if(code == '02_off_on'):
			myrelay.relay_off_on(2)
			
		if(code == '03_off'):
			myrelay.relay_off(3)
		if(code == '03_on'):
			myrelay.relay_on(3)
		if(code == '03_off_on'):
			myrelay.relay_off_on(3)
			
		if(code == '04_off'):
			myrelay.relay_off(4)
		if(code == '04_on'):
			myrelay.relay_on(4)
		if(code == '04_off_on'):
			myrelay.relay_off_on(4)
			
		if(code == '05_off'):
			myrelay.relay_off(5)
		if(code == '05_on'):
			myrelay.relay_on(5)
		if(code == '05_off_on'):
			myrelay.relay_off_on(5)
			
		if(code == '06_off'):
			myrelay.relay_off(6)
		if(code == '06_on'):
			myrelay.relay_on(6)
		if(code == '06_off_on'):
			myrelay.relay_off_on(6)
			
		if(code == '07_off'):
			myrelay.relay_off(7)
		if(code == '07_on'):
			myrelay.relay_on(7)
		if(code == '07_off_on'):
			myrelay.relay_off_on(7)
			
		if(code == '08_off'):
			myrelay.relay_off(8)
		if(code == '08_on'):
			myrelay.relay_on(8)
		if(code == '08_off_on'):
			myrelay.relay_off_on(8)

		
		return self.render_GET(request)

with open("/home/pi/python_apps/relay_server/relay.conf") as fp:
	max = len(captions)
	i = 0
	for line in fp:
		captions[i] = line
		i += 1
		if i >= max:
			break	

root = Resource()
root.putChild("relay".encode("utf-8"), RelayPage())
factory = Site(root)
endpoint = endpoints.TCP4ServerEndpoint(reactor, 31080)
endpoint.listen(factory)
reactor.run()
